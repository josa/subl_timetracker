import glob
import json
import sublime
import sublime_plugin
import os
import time
from datetime import datetime
import operator
import re

from pprint import pformat



# class MySpecialDoubleclickCommand(sublime_plugin.TextCommand):
#   def run_(self, args): 
# 
#     print self.view.substr(376)
#     timeTracker = TimeTracker.instance()
# 
#     for s in reversed(self.view.sel()):
#         str = self.view.substr(s)
#         print "| " + str
#         if s.a == 376 and s.b == 376:
#             timeTracker.open_dump()
# 
#     
#     if self.view.file_name() == timeTracker.dump_filename():
#         print "test"
# 
#     system_command = args["command"] if "command" in args else None
#     if system_command:
#         system_args = dict({"event": args["event"]}.items() + args["args"].items())
#         self.view.run_command(system_command, system_args)  
#         



class TimeTrackerShowCommand(sublime_plugin.WindowCommand):
    def run(self):
        timeTracker = TimeTracker.instance()
        timeTracker.open_dump()

class TimeTrackerRemoveCommand(sublime_plugin.WindowCommand):
    remove_path = None
    files = None
    def run(self):
        timeTracker = TimeTracker.instance()

        self.files = []

        self.remove_path = timeTracker.data_dir()
        for infile in glob.glob( os.path.join(self.remove_path, '*.json') ):

            self.files.append(re.search('([^/]*)\.json$', infile).group(1))

        self.files = sorted(self.files)

        self.window.show_quick_panel(self.files, self.on_done)

    def on_done(self, selected_index):
        if selected_index >= 0:
            try:
                file = self.files[selected_index]
                print file
                os.remove(os.path.join(self.remove_path, file + ".json"))
            except Exception, e:
                pass         


class TimeTrackerShowDayCommand(sublime_plugin.WindowCommand):
    remove_path = None
    files = None
    def run(self):
        timeTracker = TimeTracker.instance()

        self.files = []

        self.remove_path = timeTracker.data_dir()
        for infile in glob.glob( os.path.join(self.remove_path, '*.json') ):
            self.files.append(re.search('([^/]*)\.json$', infile).group(1))

        self.files = self.files
        self.files.sort()
        self.files.reverse()

        self.window.show_quick_panel(self.files, self.on_done)

    def on_done(self, selected_index):
        if selected_index >= 0:
            try:
                timeTracker = TimeTracker.instance()
                timeTracker.open_dump_day(self.files[selected_index])
            except Exception, e:
                pass


class TimeTrackerShowFilteredCommand(sublime_plugin.WindowCommand):
    projects = None
    def run(self):
        timeTracker = TimeTracker.instance()
        self.projects = timeTracker.get_projects_all()

        self.window.show_quick_panel(self.projects, self.on_done)

    def on_done(self, selected_index):

        try:
            timeTracker = TimeTracker.instance()
            timeTracker.open_dump_filtered(self.projects[selected_index])


        except Exception, e:
            pass

class TimeTrackerListener(sublime_plugin.EventListener):
    def on_post_save(self, view):
        timeTracker = TimeTracker.instance()
        timeTracker.on_post_save()

    def on_query_context(self, view, key, operator, operand, match_all):
        print "on_query_context"
        print key
        return False


class TimeTracker:

    _instance = None

    @staticmethod
    def instance():
        if not TimeTracker._instance:
            TimeTracker._instance = TimeTracker()
        return TimeTracker._instance

    def settings_get(self, key, default_value):
         settings = sublime.load_settings("SublimeTimeTracker.sublime-settings")
         return settings.get(key, default_value)

    # def on_activated(self, view):
    #     self.track_time();
    #
    # def on_deactivated(self, view):
    #     self.track_time();
    #

    def on_post_save(self):
        # self.read_data()
        # 
        self.track_time();


    def read_data(self, fname = None):

        if fname == None:
            fname = self.data_filename()

        if os.path.exists(fname):

            try:
                json_data=open(fname)
                data = json.load(json_data)
                json_data.close()

                return data
            except Exception, e:
                print(e)

        
        return None

    def read_data_today(self):
        d = self.read_data()
        if d == None:
            d = {"last":0, "tracked_time":{}}
        self.data=d


    def data_filename(self):

        fname = time.strftime("%Y-%m-%d.json")

        return self.data_dir() + fname;

    def dump_filename(self):
        return self.data_dir() + "dump.time-tracker-dump";

    def data_dir(self):

        dname = sublime.packages_path() + "/SublimeTimeTracker/data/"

        if not os.path.exists(dname):
            os.makedirs(dname)

        return dname

    def write_data(self):

        file = open(self.data_filename(), 'w+')
        file.write(json.dumps(self.data))
        file.close()

    def write_dump(self, dump):

        file = open(self.dump_filename(), 'w')
        file.write(dump)
        file.close()


    def track_time(self):
       
        project_name = self.project_name();

        self.read_data_today()

        data = self.data

        last_project_name = ''
        if "last_project" in data:
            last_project_name = data["last_project"]
        


        now = int(time.time());
        last = data["last"]

        if last > 0:
            d = now - last
        else:
            d = 0;


        m = int(time.strftime("%M"))%15
        s = int(time.strftime("%S"))

        time_slice = (now - (m * 60 + s))
        time_slice = str(time_slice)


        if last_project_name == project_name:
            if time_slice in data["tracked_time"]:
                time_slice_data = data["tracked_time"][time_slice]

                if project_name in time_slice_data:
                    time_slice_data[project_name] = time_slice_data[project_name] + d
                else:
                    time_slice_data[project_name] = d

            else:
                time_slice_data = {project_name: d};


            data["tracked_time"][str(time_slice)] = time_slice_data;

        # save data
        data["last_project"] = project_name;
        data["last"] = now

        self.write_data()


    def project_name(self):
        project_name = sublime.active_window().active_view().settings().get("project_name")

        if project_name == None:
            dir_name = self.current_dir()
            if dir_name:
                project_name = dir_name

        if project_name == None:
            project_name = 'Unknown'

        return self.map_project(project_name.encode('ascii'))

    def current_dir(self):
        folders = sublime.active_window().folders()

        if len(folders) > 0:
            return folders[0]

    def gen_divider(self):
        row = []
        for i, row_width in enumerate(self.row_widths):
            row.append("-"*row_width)
        
        return ' + '  +' + '.join(row) + " +\n"

    def gen_row(self, row_columns):

        dump_row = []
        for i, row_width in enumerate(self.row_widths):
            try:
                dump_row.append(row_columns[i].ljust(row_width))
            except Exception, e:
                dump_row.append(" "*row_width)

        return " | " + " | ".join(dump_row) + " |\n"

    def gen_menu(self, filter):
        m = " [x] " + filter +  "\n"
        return m

    def map_project(self, project_name):

        mappings = self.settings_get("project_mapping_dirs", {})

        if(project_name in mappings):
            return mappings[project_name]


        mapping_patterns = self.settings_get("project_mapping_patterns", [])
        for mapping_pattern in mapping_patterns:

            p = re.compile(mapping_pattern)
            m = p.search(project_name)

            if m != None:
                return m.group(1)

        return project_name


    def dump_tracked_time_all(self):

        first = True 
        d = self.generate_header()

        d += "\n"
        for infile in glob.glob( os.path.join(self.data_dir(), '*.json') ):
            data = self.read_data(infile)
            d += self.get_tracked_time(data, first)
            d += "\n"

        self.write_dump(d)

    def dump_tracked_time_day(self, day):

        first = True 
        d = self.generate_header()

        d += "\n"
        infile = os.path.join(self.data_dir(), day + '.json')

        data = self.read_data(infile)
        d += self.get_tracked_time(data, first)
        d += "\n"

        self.write_dump(d)

    def dump_tracked_time_filtered(self, filter):

        first = True 
        d = self.generate_header()

        d += "\n"
        d += self.gen_menu(filter)

        path = self.data_dir()
        for infile in glob.glob( os.path.join(path, '*.json') ):
            data = self.read_data(infile)
            
            dr = self.get_tracked_time(data, first, filter)
            if(dr):
                d += dr
                d += "\n"

        self.write_dump(d)


    
    def get_projects_all(self):

        projects = []

        path = self.data_dir()
        for infile in glob.glob( os.path.join(path, '*.json') ):
            data = self.read_data(infile)

            
            tracked_time = data["tracked_time"]
            tracked_time_slotes = sorted(tracked_time.iterkeys())

            for key in tracked_time_slotes:

                proj_tracked = tracked_time[key]
                project_name = max(proj_tracked.iteritems(), key=operator.itemgetter(1))[0]
                
                if project_name not in projects:
                    projects.append(project_name)


        return projects
            

        # self.write_dump(d)


    def get_tracked_time(self, data = None, first_divider = True, filter = False):

        if data == None:
            data = self.data

        tracked_time = data["tracked_time"]

        self.row_widths = [10,5,5,45] # timespend hidden

        last_project = ""
        last_date = ""
        last_timestamp = 0

        tracked_time_slotes = sorted(tracked_time.iterkeys())

        time_end_i = 2
        project_i = 3
        time_spend_i = 4


        dump_rows = []

        for key in tracked_time_slotes:

            proj_tracked = tracked_time[key]
            project_name = max(proj_tracked.iteritems(), key=operator.itemgetter(1))[0]

            if filter and project_name != filter:
                continue;

            c_timestamp = float(key)
            c_date = time.strftime("%d.%m.%Y", time.localtime(c_timestamp))
            c_time = time.strftime("%H:%M", time.localtime(c_timestamp))
            c_time_end = time.strftime("%H:%M", time.localtime(c_timestamp+(15*60)))
            
            d_time = c_timestamp - last_timestamp

            c_row = []

            if last_project == project_name and d_time == 15*60:

                c_row = dump_rows[len(dump_rows)-1]
                c_row[time_end_i] = c_time_end
                c_row[time_spend_i] = str(int(c_row[time_spend_i]) + 15*60)
               
            else:

                if last_date != c_date:
                    c_row.append(c_date)
                else:
                    c_row.append("")
                
                c_row.append(c_time)
                c_row.append(c_time_end)
                c_row.append(project_name)

                c_row.append(15*60)

                self.row_widths[project_i] = max(len(project_name), self.row_widths[project_i]);
               
                dump_rows.append(c_row)

            last_project = project_name
            last_date = c_date
            last_timestamp = c_timestamp
            
        d = ""
        if first_divider and len(dump_rows) > 0:
            d += self.gen_divider() 
           
        for c_row in dump_rows:
            d += self.gen_row(c_row)
            d += self.gen_divider()   
        return d

    def generate_header(self):
            d  = ""
            d += "   _____ _                  _____               _             \n"
            d += "  |_   _(_)_ __ ___   ___  |_   _| __ __ _  ___| | _____ _ __ \n"
            d += "    | | | | '_ ` _ \ / _ \   | || '__/ _` |/ __| |/ / _ \ '__|\n"
            d += "    | | | | | | | | |  __/   | || | | (_| | (__|   <  __/ |   \n"
            d += "    |_| |_|_| |_| |_|\___|   |_||_|  \__,_|\___|_|\_\___|_|   \n"
            return d

    def open_dump_filtered(self, project_name):
        self.dump_tracked_time_filtered(project_name)
        self.open_dump_view()

    def open_dump_day(self, day):
        self.dump_tracked_time_day(day)
        self.open_dump_view()

    def open_dump(self):
        self.dump_tracked_time_all()
        self.open_dump_view()

    def open_dump_view(self):
        win = sublime.active_window();
        v = win.open_file(self.dump_filename())
        v.set_read_only(True)
        v.run_command('revert');

        s = v.settings()
        s.set('rulers', [0,80])
        s.set('word_wrap', False)
        s.set('wrap_width', 0)
        s.set('line_numbers', False)

        win.focus_view(v)


